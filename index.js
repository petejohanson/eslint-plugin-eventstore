module.exports = {
  environments: {
    projection: {
      globals: {
        options: false,
        fromAll: false,
        fromCategory: false,
        fromStream: false,
        fromStreams: false,
        fromStreamsMatching: false,
        emit: false,
        linkTo: false
      }
    }
  }
}
