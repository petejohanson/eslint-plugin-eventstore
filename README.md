# EventStore ESLint Plugin

This plugin adds the necessary basics for the exposed globals for EventStore
user-defined projects.

## Installing

The following barebones `.eslint.json` file shows how to add this plugin:

```json
{
    "plugins": ["eventstore"],
    "env": {
        "eventstore/projection": true
    }
}
```
